<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>mymarket.ge</title>
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Noto+Sans+Georgian:wght@200;300;400&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles/header.css">
    <link rel="stylesheet" href="styles/general.css">
    <link rel="shortcut icon" href="photos/market-shortcut.png">
</head>
<body>
    
    <header id="scroll-up">
        <div class="left-section">
            <div class="logo-img">
                <button class="mymarket-button" onclick=" window.open('index.php','_self')">
                    <img class="logo-photo" src="photos/Logo-market.0703513661f2ed689aaf.svg" alt="">
                </button>
            </div>
        </div>


        <div class="middle-section">

            <div class="input">
                <input class="search-bar" type="text" id="search" placeholder="mymarket" onfocus="changePlaceholder()" onfocusout="changePlaceholder1()">
                <button class="search-button">
                    <img class="search-icon" src="photos/camera-icon.svg" alt="">
                </button>
                <button class="search-button1">
                    <img class="search-icon" src="photos/search-icon.svg" alt="">
                </button>
            </div>
            
            <button class="add">
                <img class="add-photo" src="photos/add.svg" alt="">
                <p class="add-text">დამატება</p>
            </div>

            
        </div>

        <div class="small-icons">

            <button class="icon-button search2">
                <img class="icons" src="photos/search-icon.svg" alt="">
            </button>

            <button class="icon-button chanacvleba">
                <img class="icons" src="photos/notification.svg" alt="">
            </button>
            
            <button class="icon-button">
                <img class="icons" src="photos/favorite.svg" alt="">
            </button>

            <button class="icon-button" onclick="window.location.href = 'kalata.php'; return false;">
                <img class="icons" src="photos/kalata.svg" alt="">
            </button>
            
        </div>

        <div class="right-section">

            <div class="small-icons small-icons2">

                <button class="icon-button search2">
                    <img class="icons" src="photos/search-icon.svg" alt="">
                </button>
    
                <button class="icon-button chanacvleba">
                    <img class="icons" src="photos/notification.svg" alt="">
                </button>
                
                <button class="icon-button">
                    <img class="icons" src="photos/favorite.svg" alt="">
                </button>
    
                <button class="icon-button" onclick=" window.open('kalata.php','_blank')">
                    <img class="icons" src="photos/kalata.svg" alt="">
                </button>
                
            </div>

            <button onclick="window.location.href = 'styles/page2.php';" class="avtorizacia">
                <img src="photos/avtorizacia.svg" alt="">
                <p class="avtorizacia-text">ავტორიზაცია</p>
            </button>

            <div class="last">
                <button class="icon-button hamburger-menu">
                    <img class="hamburger-icon" src="photos/hambrger-menu.svg" alt="">
                </button>
                <div class="drop-down">
                    <form action="">
                    <button class="page" onclick="window.location.href = 'styles/page1.php'; return false;">რეგისტრაცია</button>
                    <br>
                    <button class="page" onclick="window.location.href = 'daxmareba.php'; return false;">დახმარება</button>
                    <br>
                    <button class="page" onclick="window.location.href = 'contact.php'; return false;">კონტაქტი</button>
                    </form>
                </div>
                <img class="qr-code" src="photos/qr-code.svg" alt="">
            </div>
        </div>
    </header>

    <div class="line"></div>

    <div class="header2">
        <a href="" class="buy buy2">ონლაინ ყიდვა</a>
        <a href="" class="buy buy1">კატეგორიები</a>
        <div class="left-section">
            <button class="add sell-ofer">
                <img src="photos/pasdakleba.svg" alt="">
                <p>ფასდაკლება</p>
            </button>
        </div>
        
    </div>

    <main>
        <div class="first-div">
            <div class="categori">
                <p class="categori-text">საახალწლო პროდუქცია</p>
                <p class="categori-text">მომსახურება</p>
                <p class="categori-text">გაქირავება</p>
                <p class="categori-text">სახლი და ბაღი</p>
                <p class="categori-text">საოჯახო ტექნიკა</p>
                <p class="categori-text">ტექნიკა</p>
                <p class="categori-text">ნადირობა და თევზაობა</p>
                <p class="categori-text">მუსიკა</p>
                <p class="categori-text">საბავშვო</p>
                <p class="categori-text">სილამაზე და მოდა</p>
                <p class="categori-text">მშენებლობა და რემონტი</p>
                <p class="categori-text">სოფლის მეურნეობა</p>
                <p class="categori-text">ცხოველები</p>
                <p class="categori-text">სპორტი და დასვენება</p>
                <p class="categori-text">ბიზნესი და დანადგარები</p>
                <p class="categori-text">წიგნები და კანცელარია</p>
                <p class="categori-text">ხელოვნება და საკოლექციო</p>

                <img class="apat-logo" src="photos/apataravebs.svg" alt="">
            </div>

            <div class="next-to-categori">
                <div class="offers">
                    <a href="styles/page2.php" class="sighn-in-a">
                        <div class="offer-div off1">
                            <img class="offer-logo" src="photos/offer1.svg" alt="">
                            <p class="offer-text">შესვლა/</p>
                            <p class="offer-text">რეგისტრაცია</p>
                        </div>
                    </a>
                    
                    <div class="offer-div off1">
                        <img class="offer-logo" src="photos/offer2.svg" alt="">
                        <p class="offer-text">ფასდაკლებები</p>
                    </div>

                    <div class="offer-div off1">
                        <img class="offer-logo" src="photos/offer3.svg" alt="">
                        <p class="offer-text">ონლაინ მაღაზიები</p>
                    </div>

                    <div class="offer-div off1">
                        <p class="offer-text">საახალწლო</p>
                        <p class="offer-text">პროდუქცია</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="continue-search">
            <p class="continue-text">გააგრძელე ძიება</p>
            <form action="">
                <button class="continue-button">ბოლო</button>
                <button class="continue-button continue-button1">შენახული</button>
            </form>
        </div>

        <div class="saaxalwlo-produqcia">
            <div class="saaxalwlo-div">
                <p>საახალწლო პროდუქცია</p>
                <img class="saaxalwlo-logo" src="photos/favorite2.svg" alt="">
            </div>
        </div>

        <div class="trade">
            <img class="trade-photo" src="photos/trade.gif" alt="">

            <div class="trade-text">
                <p class="text1">Trade in - შეცვალე ძველი ტექნიკა ახლით</p>
                <p class="text2">ატვირთე შენი ძველი ტექნიკის მონაცემები, ჩააბარე და დაიბრუნე მისი ღირებულების საბანკო ანგარიშზე</p>
                <button class="start"><p>დაწყება</p></button>
            </div>
        </div>

        <div class="vip">
            <div class="super-vip">
                <button class="vip-button">s-vip</button>
                <p class="vip-text">SUPER VIP</p>
                <a href=""><p class="gancxadeba">193 განცხადება</p></a>
            </div>

            <div class="vip-product">
                <img class="vip-photo" src="photos/11.jpg" alt="">
                <p class="vip-title">მაკიაჟის და...</p>
                <p class="vip-price">350.0 ₾</p>
            </div>

            <div class="vip-product">
                <img class="vip-photo" src="photos/vip1.jpg" alt="">
                <p class="vip-title">რადიატორი</p>
                <p class="vip-price">100.0 ₾</p>
            </div>

            <div class="vip-product">
                <img class="vip-photo" src="photos/12.jpg" alt="">
                <p class="vip-title">I5_ის პროცესო...</p>
                <p class="vip-price">ფასი შეთანხმებით</p>
            </div>

            <div class="vip-product">
                <img class="vip-photo" src="photos/13.jpg" alt="">
                <p class="vip-title">გაზის...</p>
                <p class="vip-price">ფასი შეთანხმებით</p>
            </div>

            <div class="vip-product vip-product2">
                <img class="vip-photo" src="photos/14.jpg" alt="">
                <p class="vip-title">მტვერსასრუტი</p>
                <p class="vip-price">550.00 ₾</p>
            </div>

            <div class="vip-product vip-product2">
                <img class="vip-photo" src="photos/15.jpg" alt="">
                <p class="vip-title">საგაზო...</p>
                <p class="vip-price">15.25 ₾</p>
            </div>

            <div class="vip-product vip-product2">
                <img class="vip-photo" src="photos/16.jpg" alt="">
                <p class="vip-title">I3_ის პროცესო...</p>
                <p class="vip-price">ფასი შეთანხმებით</p>
            </div>

            <div class="vip-product vip-product2">
                <img class="vip-photo" src="photos/17.jpg" alt="">
                <p class="vip-title">მალტიპუს...</p>
                <p class="vip-price">1200.00 ₾</p>
            </div>
        </div>

        <div class="trade">
            <div class="trade-text">
                <p class="text1">იყიდე მეორადი ნივთები სახლიდან</p>
                <p class="text2">უკვე შეგიძლია, მეორადი პროდუქცია იყიდო სახლიდან გაუსვლელად, ონლაინ გადახდითა და ადგილზე მიტანით.
                </p>
                <button class="start start1"><p>იყიდე ახლავე</p></button>
            </div>

            <img class="trade-photo" src="photos/meoradi.gif" alt="">

            <button class="start start2"><p>იყიდე ახლავე</p></button>
        </div>

    
        <div class="category category2">
            <a href="showproducts.php" class="categori-a">
                <div class="category-photo category-photo2">
                    <div class="div-photo div-photo2">
                        <img class="category-photo" src="photos/cat1.png" alt="">
                    </div>
                    <p class="category-text">ტექნიკა</p>
                </div>
            </a>
            
            <a href="showproducts.php" class="categori-a">
                <div class="category-photo category-photo2">
                    <div class="div-photo">
                        <img class="category-photo" src="photos/cat2.png" alt="">
                    </div>
                    <p class="category-text">საოჯახო ტექნიკა</p>
                </div>
            </a>
          
            <a href="showproducts.php" class="categori-a">
                <div class="category-photo category-photo2">
                    <div class="div-photo">
                        <img class="category-photo" src="photos/cat5.png" alt="">
                    </div>
                    <p class="category-text">სილამაზე და მოდა</p>
                </div>
            </a>

            <a href="showproducts.php" class="categori-a">
            <div class="category-photo category-photo2">
                <div class="div-photo">
                    <img class="category-photo" src="photos/cat3.png" alt="">
                </div>
                <p class="category-text">მშენებლობა და რემონტი</p>
            </div>
            </a>

            <a href="showproducts.php" class="categori-a">
            <div class="category-photo category-photo2">
                <div class="div-photo">
                    <img class="category-photo" src="photos/cat4.png" alt="">
                </div>
                <p class="category-text">სახლი და ბაღი</p>
            </div>
            </a>

            <a href="showproducts.php" class="categori-a">
            <div class="category-photo category-photo2">
                <div class="div-photo">
                    <img class="category-photo" src="photos/cat6.png" alt="">
                </div>
                <p class="category-text">ბიზნესი და დანადგარები</p>
            </div>
            </a>   
        </div>

        <div class="foryou foryou2">
            <div class="left left2">
                <p class="continue-text continue-text2">შენთვის საუკეთესო</p>
                <button class="continue-button g1">Dekstop კმპიუტერ...</button>
            </div>
        </div>

        <div class="vip foryou-div foryou-div2">
            <div class="vip-product vip-product3">
                <img class="vip-photo foryou-photo foryou-photo2" src="photos/comp1.jpg" alt="">
                <p class="vip-title">სასწრაფოდ იყიდება...</p>
                <p class="vip-price">5300.00 ₾</p>
            </div>

            <div class="vip-product vip-product3">
                <img class="vip-photo foryou-photo foryou-photo2" src="photos/comp2.jpg" alt="">
                <p class="vip-title">27-იანი SAMSUNG მონიტო...</p>
                <p class="vip-price">2990.00 ₾</p>
            </div>

            <div class="vip-product vip-product3">
                <img class="vip-photo foryou-photo foryou-photo2" src="photos/comp3.jpg" alt="">
                <p class="vip-title">ნებისმიერი სიძლიერის...</p>
                <p class="vip-price">25100.00 ₾</p>
            </div>

            <div class="vip-product vip-product3">
                <img class="vip-photo foryou-photo foryou-photo2" src="photos/comp4.jpg" alt="">
                <p class="vip-title">i5 10400 / 8GB RAM / RX 470 /...</p>
                <p class="vip-price">1250.00 ₾</p>
            </div>

            <div class="vip-product vip-product2">
                <img class="vip-photo foryou-photo foryou-photo2" src="photos/comp5.jpg" alt="">
                <p class="vip-title">უძლიერესი სარენდერო /...</p>
                <p class="vip-price">5800.00 ₾</p>
            </div>
        </div>

        <div class="trade">
            <div class="trade-text">
                <p class="text1">ონლაინ მაღაზიები</p>
                <p class="text2">შეიძინე ონლაინ საშენო პროდუქცია, 300-ზე მეტი მაღაზიისგან და მიიღე სასურველ მისამართზე მთელს საქართველოში
                </p>
                <button class="start start1">დაიწყე შოპინგი</button>
            </div>

            <img class="trade-photo" src="photos/onlineshop.gif" alt="">
            <button class="start start2"><p>დაიწყე შოპინგი</p></button>
        </div>

        <div class="continue-search">
            <p class="continue-text">საშენო პროდუქტები</p>
            <form action="">
                <button class="continue-button">წიგნები</button>
                <button class="continue-button continue-button1">თავის მოვლის საშუ...</button>
            </form>
        </div>

        <div class="books books2">
            <div class="book-img book-img2">
                <img class="book-photo" src="photos/books.jpg" alt="">
                <p class="book-text">წიგნები</p>
            </div>

            <div class="book-product book-product2">
                <div class="list list3">
                    <img class="book-img" src="photos/book1.jpg" alt="">
    
                    <div class="about-book">
                        <p class="book-title">ოცნება თამაშგარე მდგომარეობაში</p>
                        <p class="book-price">9.95 ₾</p>
                    </div>
                </div>
    
                <div class="list list3">
                    <img class="book-img" src="photos/book2.jpg" alt="">
    
                    <div class="about-book">
                        <p class="book-title">ფანტასტიკური ბიბლიოთეკა - განპყრობილები</p>
                        <p class="book-price">18.90 ₾</p>
                    </div>
                </div>

                <div class="list list3">
                    <img class="book-img" src="photos/book3.jpg" alt="">
    
                    <div class="about-book">
                        <p class="book-title">ფანტასტიკური ბიბლიოთეკა - კოსმოსური ოდისეა...</p>
                        <p class="book-price">15.90 ₾</p>
                    </div>
                </div>

                <div class="list list2 list3">
                    <div class="about-book about2">
                        <p class="show-all">მაჩვენე ყველა</p>
                        <p class="gancxadeba raodenoba">755 განცხადება</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="shops">
            <p class="shops-text">მაღაზიები</p>
            <a href=""><p class="gancxadeba">575 მაღაზია</p></a>
        </div>

        <div class="vip foryou-div shops3">
            <div class="vip-product shop-product">
                <img class="vip-photo shops-photo" src="photos/shop1.jpg" alt="">
                <p class="vip-title shop-title">GPS OF GEORGIA</p>
            </div>

            <div class="vip-product shop-product">
                <img class="vip-photo shops-photo" src="photos/shop2.jpg" alt="">
                <p class="vip-title shop-title">CAUCASUS...</p>
            </div>

            <div class="vip-product shop-product">
                <img class="vip-photo shops-photo" src="photos/shop3.jpg" alt="">
                <p class="vip-title shop-title">ჭკვიანი სახლი</p>
            </div>

            <div class="vip-product shop-product">
                <img class="vip-photo shops-photo" src="photos/shop4.jpg" alt="">
                <p class="vip-title shop-title shop-titlecc">GEO ENERGY EXPERT</p>
            </div>

            <div class="vip-product shop-product shop-product3">
                <img class="vip-photo shops-photo" src="photos/shop5.jpg" alt="">
                <p class="vip-title shop-title">NAILMARKET...</p>
            </div>

            <div class="vip-product shop-product shop-product3">
                <img class="vip-photo shops-photo" src="photos/shop6.jpg" alt="">
                <p class="vip-title shop-title">ADJARA PEAK</p>
            </div>

            <div class="vip-product shop-product shop-product3">
                <img class="vip-photo shops-photo" src="photos/shop7.jpg" alt="">
                <p class="vip-title shop-title">მეტალოპლასტმას...</p>
            </div>

            <div class="vip-product shop-product shop-product3">
                <img class="vip-photo shops-photo" src="photos/shop8.jpg" alt="">
                <p class="vip-title shop-title">THE BABY</p>
            </div>
        </div>

        <div class="shops">
            <p class="shops-text">ფასდაკლებები</p>
        </div>
        
        <div class="sells">
            <div class="sells1">
                <div class="item1 container container3">
                    <img class="sells-photo sells-photo3" src="photos/sells1.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">1189.03 ₾</div>
                        <div class="price2">1399.00 ₾</div>
                    </div>
                </div>
    
                <div class="item1 container container3">
                    <img class="sells-photo sells-photo3" src="photos/sells2.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">1159.03 ₾</div>
                        <div class="price2">1369.00 ₾</div>
                    </div>
                </div>
    
                <div class="item1 container container3">
                    <img class="sells-photo sells-photo3"  src="photos/sells3.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">2379.00 ₾</div>
                        <div class="price2">2896.00 ₾</div>
                    </div>
                </div>
    
                <div class="item1 container container3">
                    <img class="sells-photo sells-photo3" src="photos/sells4.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">1079.00 ₾</div>
                        <div class="price2">1579.00 ₾</div>
                    </div>
                </div>

                <div class="sells-title">
                    <div class="procenti">-11%</div>
                    <p class="sells-name">საოჯახო ტექნიკა</p>
                    <a href=""><p class="gancxadeba">122 განცხადება</p></a>
                </div>    
            </div>

            
            <div class="sells1 sells3">
                <div class="item1 container">
                    <img class="sells-photo" src="photos/s1.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">18.00 ₾</div>
                        <div class="price2">20.00 ₾</div>
                    </div>
                </div>
    
                <div class="item1 container">
                    <img class="sells-photo" src="photos/s2.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">18.00 ₾</div>
                        <div class="price2">20.00 ₾</div>
                    </div>
                </div>
    
                <div class="item1 container">
                    <img class="sells-photo" src="photos/s3.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">18.00 ₾</div>
                        <div class="price2">20.00 ₾</div>
                    </div>
                </div>
    
                <div class="item1 container">
                    <img class="sells-photo" src="photos/s4.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">14.40 ₾</div>
                        <div class="price2">18.00 ₾</div>
                    </div>
                </div>

                <div class="sells-title">
                    <div class="procenti">-11%</div>
                    <p class="sells-name">საოჯახო ტექნიკა</p>
                    <a href=""><p class="gancxadeba">21 განცხადება</p></a>
                </div>
            </div>

            <div class="sells1 sells3">
                <div class="item1 container">
                    <img class="sells-photo" src="photos/a1.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">72.45 ₾</div>
                        <div class="price2">1399.00 ₾</div>
                    </div>
                </div>
    
                <div class="item1 container">
                    <img class="sells-photo" src="photos/a2.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">82.35 ₾</div>
                        <div class="price2">91.50 ₾</div>
                    </div>
                </div>
    
                <div class="item1 container">
                    <img class="sells-photo" src="photos/a3.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">72.45 ₾</div>
                        <div class="price2">89.59 ₾</div>
                    </div>
                </div>
    
                <div class="item1 container">
                    <img class="sells-photo" src="photos/a4.jpg" alt="">
                    <div class="overlay">
                        <div class="price1">72.45 ₾</div>
                        <div class="price2">89.59 ₾</div>
                    </div>
                </div>

                <div class="sells-title">
                    <div class="procenti">-11%</div>
                    <p class="sells-name">საოჯახო ტექნიკა</p>
                    <a href=""><p class="gancxadeba">575 განცხადება</p></a>
                </div>
            </div>
        </div>

        <div class="shops momsaxureba">
            <p class="shops-text">მომსახურება</p>
            <a href=""><p class="gancxadeba">3378 განცხადება</p></a>
        </div>

        <div class="moms-offer">
            <div class="list list4">
                <img class="moms-img" src="photos/moms1.jpg" alt="">

                <div class="about-book">
                    <p class="moms-type">ტვირთების გადატანა</p>
                    <p class="moms-title">ავეჯის გადაზიდვა, გადატანა...</p>
                    <p class="moms-price">ფასი შეთანხმებით</p>
                </div>
            </div>

            <div class="list list4">
                <img class="moms-img" src="photos/moms2.jpg" alt="">

                <div class="about-book">
                    <p class="moms-type">ტვირთების გადატანა</p>
                    <p class="moms-title">პიანინოს და როიალის გადაზიდვ...</p>
                    <p class="moms-price">7.00 ₾</p>
                </div>
            </div>

            <div class="list list4">
                <img class="moms-img" src="photos/moms3.jpg" alt="">

                <div class="about-book">
                    <p class="moms-type">ტვირთების გადატანა</p>
                    <p class="moms-title">ავეჯის გადაზიდვა/ტვირთის...</p>
                    <p class="moms-price">30.00 ₾</p>
                </div>
            </div>
        </div>  
        
        <div class="last-part">
            <div class="how">
                <div class="buy-online">
                    <p class="shops-text question">როგორ ვიყიდო ონლაინ?</p>
                </div>
                <div class="explane">
                    <div class="way">
                        <div class="animation">
                            <img class="explane-img" src="photos/way1.png" alt="">
                        </div>
                        <p class="explane-text">დააკლიკე ღილაკს "ონლაინ ყიდვა"</p>
                    </div>

                    <div class="way">
                        <img src="photos/way2.png" alt="">
                        <p class="explane-text">ნახე სასურველ კატეგორიაში საშენო პროდუქტი</p>
                    </div>

                    <div class="way">
                        <img src="photos/way3.png" alt="">
                        <p class="explane-text">დააჭირე ღილაკს „შეძენა“ და მიყევი ინსტრუქციას.</p>
                    </div>

                    <div class="way">
                        <div class="animation">
                            <img class="explane-img" src="photos/way4.png" alt="">
                        </div>
                        <p class="explane-text">ნივთს ჩვენ მოგიტანთ სასურველ მისამართზე</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="scroll-up">
            <a href="#scroll-up"><button class="scroll">^</button></a>
        </div>
    </main>

    <footer>
        <div class="footer-div">
            <div class="top-category">
                <div class="section1">
                    <p class="footer-title">ტოპ კატეგორიები</p>
                    <p class="footer-text">საახალწლო პროდუქცია</p>
                    <p class="footer-text">მომსახურება</p>
                    <p class="footer-text">გაქირავება</p>
                    <p class="footer-text">სახლი და ბაღი</p>
                    <p class="footer-text">საოჯახო ტექნიკა</p>
                    <p class="footer-text">ტექნიკა</p>
                </div>
                <div class="section2">
                    <p class="footer-text">ნადირობა და თევზაობა</p>
                    <p class="footer-text">მუსიკა</p>
                    <p class="footer-text">საბავშვო</p>
                    <p class="footer-text">სილამაზე და მოდა</p>
                    <p class="footer-text">მშენებლობა და რემონტი</p>
                    <p class="footer-text">სოფლის მეურნეობა</p>
                </div>
                <div class="section2">
                    <p class="footer-text">ცხოველები</p>
                    <p class="footer-text">სპორტი და დასვენება</p>
                    <p class="footer-text">ბიზნესი და დანადგარები</p>
                    <p class="footer-text">წიგნები და კანცელარია</p>
                    <p class="footer-text">ხელოვნება და საკოლექცი</p>
                </div>      
            </div>

            <div class="menu">
                <p class="footer-title">მენიუ</p>
                <p class="footer-text">მთავარი</p>
                <p class="footer-text">მაღაზიები</p>  
                <p class="footer-text">იყიდე ონლაინ</p>  
                <p class="footer-text">დახმარება</p>  
                <p class="footer-text">დაბრუნების პოლიტიკა</p>  
                <p class="footer-text">კონტაქტი</p>  
                <p class="footer-text">წესები და პირობები</p>  
                <p class="footer-text">კონფიდენციალობის პოლი...</p>   
            </div>

            <div class="menu">
                <p class="footer-title">ჩემი გვერდი</p>
                <p class="footer-text">ჩემი განცხადებები</p>
                <p class="footer-text">განცხადების დამატება</p>  
                <p class="footer-text">შეტყობინებები</p>  
                <p class="footer-text">ბალანსის შევსება</p>  
                <p class="footer-text">ანგარიშის რედაქტირება</p>   
            </div>

            <div class="footer-bar">
                <div class="soc-networking">
                    <p class="footer-title">სოც.ქსელები</p>

                    <button class="soc-button">
                        <img src="photos/facebook_mlfu3qnrl6o8.svg" alt="" class="soc-photos">
                    </button>

                    <button class="soc-button">
                        <img src="photos/instagram_d3e4zzggndi1.svg" alt="" class="soc-photos">
                    </button>

                    <button class="soc-button">
                        <img src="photos/linkedin_a8kvri0eq463.svg" alt="" class="soc-photos">
                    </button>
                </div>

                <div class="soc-networking">
                    <p class="footer-title">ენები</p>

                    <button class="soc-button georgia">
                        <img src="photos/georgia_6j844j2zcl2u.svg" alt="" class="soc-photos language">
                    </button>

                    <button class="soc-button">
                        <img src="photos/united_kingdom_zbaex3vpz0gg.svg" alt="" class="soc-photos language">
                    </button>

                    <button class="soc-button">
                        <img src="photos/pig_kubd7rdzk2ii.svg" alt="" class="soc-photos language">
                    </button>
                </div>

                <div class="soc-networking download">
                    <p class="footer-title">გადმოწერე აპლიკაცია</p>

                    <button class="platform">
                        <img src="photos/app-store.png" alt="" class="plat-photos">
                    </button>

                    <button class="platform1">
                        <img src="photos/play-store.png" alt="" class="plat-photos1">
                    </button>
                </div>
            </div>
        </div>

        <div class="footer-bar footer-bar2">
            <div class="soc-networking">
                <p class="footer-title">სოც.ქსელები</p>

                <button class="soc-button">
                    <img src="photos/facebook_mlfu3qnrl6o8.svg" alt="" class="soc-photos">
                </button>

                <button class="soc-button">
                    <img src="photos/instagram_d3e4zzggndi1.svg" alt="" class="soc-photos">
                </button>

                <button class="soc-button">
                    <img src="photos/linkedin_a8kvri0eq463.svg" alt="" class="soc-photos">
                </button>
            </div>

            <div class="soc-networking">
                <p class="footer-title">ენები</p>

                <button class="soc-button georgia">
                    <img src="photos/georgia_6j844j2zcl2u.svg" alt="" class="soc-photos language">
                </button>

                <button class="soc-button">
                    <img src="photos/united_kingdom_zbaex3vpz0gg.svg" alt="" class="soc-photos language">
                </button>

                <button class="soc-button">
                    <img src="photos/pig_kubd7rdzk2ii.svg" alt="" class="soc-photos language">
                </button>
            </div>

            <div class="soc-networking download">
                <p class="footer-title">გადმოწერე აპლიკაცია</p>

                <button class="platform">
                    <img src="photos/app-store.png" alt="" class="plat-photos">
                </button>

                <button class="platform1">
                    <img src="photos/play-store.png" alt="" class="plat-photos1">
                </button>
            </div>
        </div>
    </footer>
    <script src="styles/main.js"></script>
</body>
</html>
