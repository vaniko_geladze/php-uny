<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>mymarket.ge</title>
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Noto+Sans+Georgian:wght@200;300;400&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles/header.css">
    <link rel="stylesheet" href="styles/kalata.css">
    <link rel="stylesheet" href="styles/header.css">
    <link rel="stylesheet" href="styles/general.css">
    <link rel="shortcut icon" href="photos/market-shortcut.png">
</head>
<body>
    <header id="scroll-up">
        <div class="left-section">
            <div class="logo-img">
                <button class="mymarket-button" onclick=" window.open('index.php','_self')">
                    <img class="logo-photo" src="photos/Logo-market.0703513661f2ed689aaf.svg" alt="">
                </button>
            </div>
        </div>


        <div class="middle-section">

            <div class="input">
                <input class="search-bar" type="text" id="search" placeholder="mymarket" onfocus="changePlaceholder()" onfocusout="changePlaceholder1()">
                <button class="search-button">
                    <img class="search-icon" src="photos/camera-icon.svg" alt="">
                </button>
                <button class="search-button1">
                    <img class="search-icon" src="photos/search-icon.svg" alt="">
                </button>
            </div>
            
            <button class="add">
                <img class="add-photo" src="photos/add.svg" alt="">
                <p class="add-text">დამატება</p>
            </div>

            
        </div>

        <div class="small-icons">
            <button class="icon-button">
                <img class="icons" src="photos/notification.svg" alt="">
            </button>
            
            <button class="icon-button">
                <img class="icons" src="photos/favorite.svg" alt="">
            </button>

            <button class="icon-button" onclick=" window.open('kalata.php','_self')">
                <img class="icons" src="photos/kalata.svg" alt="">
            </button>
            
        </div>

        <div class="right-section">
            <button onclick=" window.open('styles/page2.php','_self')" class="avtorizacia">
                <img src="photos/avtorizacia.svg" alt="">
                <p class="avtorizacia-text">ავტორიზაცია</p>
            </button>

            <div class="last">
                <button class="icon-button hamburger-menu">
                    <img class="hamburger-icon" src="photos/hambrger-menu.svg" alt="">
                </button>
                <div class="drop-down">
                    <form action="">
                    <button class="page" onclick="window.location.href = 'styles/page1.php'; return false;">რეგისტრაცია</button>
                    <br>
                    <button class="page" onclick="window.location.href = 'daxmareba.php'; return false;">დახმარება</button>
                    <br>
                    <button class="page" onclick="window.location.href = 'contact.php'; return false;">კონტაქტი</button>
                    </form>
                </div>
                <img class="qr-code" src="photos/qr-code.svg" alt="">
            </div>
        </div>
    </header>

    <main>
        <div class="kalata">
            <div class="title">
                <p class="kalata-title">
                    ჩემი კალათა
                </p>
            </div>
            <div class="kalata-box">
                <div class="shignit">
                    <img class="kalata-logo" src="photos/kalata.svg" alt="">
                    <p class="kalata-text">შენი კალათი ცარიელია</p>
                    <p class="kalata-text2">გადახედე ონლაინ მაღაზიების განცხადებებს და დაამატე სასურველი ნივთები კალათში</p>
                    <button class="submit">დაიწყე შოპინგი</button>
                </div>
            </div>
        </div>
        <div class="scroll-up">
            <a href="#scroll-up"><button class="scroll">^</button></a>
        </div>
    </main>

    <footer>
        <div class="footer-div">
            <div class="top-category">
                <div class="section1">
                    <p class="footer-title">ტოპ კატეგორიები</p>
                    <p class="footer-text">საახალწლო პროდუქცია</p>
                    <p class="footer-text">მომსახურება</p>
                    <p class="footer-text">გაქირავება</p>
                    <p class="footer-text">სახლი და ბაღი</p>
                    <p class="footer-text">საოჯახო ტექნიკა</p>
                    <p class="footer-text">ტექნიკა</p>
                </div>
                <div class="section2">
                    <p class="footer-text">ნადირობა და თევზაობა</p>
                    <p class="footer-text">მუსიკა</p>
                    <p class="footer-text">საბავშვო</p>
                    <p class="footer-text">სილამაზე და მოდა</p>
                    <p class="footer-text">მშენებლობა და რემონტი</p>
                    <p class="footer-text">სოფლის მეურნეობა</p>
                </div>
                <div class="section2">
                    <p class="footer-text">ცხოველები</p>
                    <p class="footer-text">სპორტი და დასვენება</p>
                    <p class="footer-text">ბიზნესი და დანადგარები</p>
                    <p class="footer-text">წიგნები და კანცელარია</p>
                    <p class="footer-text">ხელოვნება და საკოლექცი</p>
                </div>      
            </div>

            <div class="menu">
                <p class="footer-title">მენიუ</p>
                <p class="footer-text">მთავარი</p>
                <p class="footer-text">მაღაზიები</p>  
                <p class="footer-text">იყიდე ონლაინ</p>  
                <p class="footer-text">დახმარება</p>  
                <p class="footer-text">დაბრუნების პოლიტიკა</p>  
                <p class="footer-text">კონტაქტი</p>  
                <p class="footer-text">წესები და პირობები</p>  
                <p class="footer-text">კონფიდენციალობის პოლი...</p>   
            </div>

            <div class="menu">
                <p class="footer-title">ჩემი გვერდი</p>
                <p class="footer-text">ჩემი განცხადებები</p>
                <p class="footer-text">განცხადების დამატება</p>  
                <p class="footer-text">შეტყობინებები</p>  
                <p class="footer-text">ბალანსის შევსება</p>  
                <p class="footer-text">ანგარიშის რედაქტირება</p>   
            </div>

            <div class="footer-bar">
                <div class="soc-networking">
                    <p class="footer-title">სოც.ქსელები</p>

                    <button class="soc-button">
                        <img src="photos/facebook_mlfu3qnrl6o8.svg" alt="" class="soc-photos">
                    </button>

                    <button class="soc-button">
                        <img src="photos/instagram_d3e4zzggndi1.svg" alt="" class="soc-photos">
                    </button>

                    <button class="soc-button">
                        <img src="photos/linkedin_a8kvri0eq463.svg" alt="" class="soc-photos">
                    </button>
                </div>

                <div class="soc-networking">
                    <p class="footer-title">ენები</p>

                    <button class="soc-button georgia">
                        <img src="photos/georgia_6j844j2zcl2u.svg" alt="" class="soc-photos language">
                    </button>

                    <button class="soc-button">
                        <img src="photos/united_kingdom_zbaex3vpz0gg.svg" alt="" class="soc-photos language">
                    </button>

                    <button class="soc-button">
                        <img src="photos/pig_kubd7rdzk2ii.svg" alt="" class="soc-photos language">
                    </button>
                </div>

                <div class="soc-networking download">
                    <p class="footer-title">გადმოწერე აპლიკაცია</p>

                    <button class="platform">
                        <img src="photos/app-store.png" alt="" class="plat-photos">
                    </button>

                    <button class="platform1">
                        <img src="photos/play-store.png" alt="" class="plat-photos1">
                    </button>
                </div>
            </div>
        </div>

        <div class="footer-bar footer-bar2">
            <div class="soc-networking">
                <p class="footer-title">სოც.ქსელები</p>

                <button class="soc-button">
                    <img src="photos/facebook_mlfu3qnrl6o8.svg" alt="" class="soc-photos">
                </button>

                <button class="soc-button">
                    <img src="photos/instagram_d3e4zzggndi1.svg" alt="" class="soc-photos">
                </button>

                <button class="soc-button">
                    <img src="photos/linkedin_a8kvri0eq463.svg" alt="" class="soc-photos">
                </button>
            </div>

            <div class="soc-networking">
                <p class="footer-title">ენები</p>

                <button class="soc-button georgia">
                    <img src="photos/georgia_6j844j2zcl2u.svg" alt="" class="soc-photos language">
                </button>

                <button class="soc-button">
                    <img src="photos/united_kingdom_zbaex3vpz0gg.svg" alt="" class="soc-photos language">
                </button>

                <button class="soc-button">
                    <img src="photos/pig_kubd7rdzk2ii.svg" alt="" class="soc-photos language">
                </button>
            </div>

            <div class="soc-networking download">
                <p class="footer-title">გადმოწერე აპლიკაცია</p>

                <button class="platform">
                    <img src="photos/app-store.png" alt="" class="plat-photos">
                </button>

                <button class="platform1">
                    <img src="photos/play-store.png" alt="" class="plat-photos1">
                </button>
            </div>
        </div>
    </footer>
</body>
</html>