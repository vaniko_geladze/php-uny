<?php
    session_start();
    include "db_conection.php";

    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header("Location: index.php");
        exit();
    }

    if (isset($_GET['logout'])) {
        session_unset();
        session_destroy();
        header("Location: index.php");
        exit();
    }

    $users_quantity = 0;
    $categories_quantity = 0;
    $products_quantity = 0;
    $orders_quantity = 0;

    $select_users = "SELECT * FROM users";
    $select_categories = "SELECT * FROM categories";
    $select_products = "SELECT * FROM products";
    $select_orders = "SELECT * FROM orders";

    $result_users = mysqli_query($conn, $select_users);
    $result_categories = mysqli_query($conn, $select_categories);
    $result_products= mysqli_query($conn, $select_products);
    $result_orders = mysqli_query($conn, $select_orders);

    foreach ($result_users as $users) {
        $users_quantity++;  
    }

    foreach ($result_categories as $categories) {
        $categories_quantity++;
    }

    foreach ($result_products as $products) {
        $products_quantity++;
    }

    foreach ($result_orders as $products) {
        $orders_quantity++;
    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>admin</title>
    <link rel="stylesheet" href="styles/admin-page.css">
    <link rel="shortcut icon" href="photos/market-shortcut.png">
</head>

<header>
    <div class="nav">
    <a href="admin.php"><img src="photos/software-engineer.png" class="admin-icon"></a>
        <a href="?logout"><img src="photos/7612790.png" class="logout-icon"></a>
    </div>
</header>

<body>
    <div class="main">
        <a href="totalusers.php">
            <div class="content">
                <img src="photos/multiple-users.png" class="icons">
                <p>Total Users</p>
                <p><?php echo $users_quantity;?></p> 
            </div>
        </a>
        
        <a href="totalcategories.php">
            <div class="content">
                <img src="photos/categories.png" class="icons">
                <p>Total Categories</p>
                <p><?php echo $categories_quantity;?></p>
            </div>
        </a>
        <a href="totalproducts.php">
            <div class="content">
                <img src="photos/cubes.png" class="icons">
                <p>Total Products</p>
                <p><?php echo $products_quantity;?></p>
            </div>
        </a>
        
        <a href="totalorders.php">
            <div class="content">
                <img src="photos/orders-icon.png" class="icons">
                <p>Total Orders</p>
                <p><?php echo $orders_quantity;?></p>
            </div>
        </a>
    </div>
</body>
</html>