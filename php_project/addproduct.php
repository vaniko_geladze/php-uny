<?php
    session_start();
    $post_data = isset($_SESSION['post_data']) ? $_SESSION['post_data'] : array();
    include "db_conection.php";
    
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header("Location: index.php");
        exit();
    }

    if (isset($_GET['logout'])) {
        session_unset();
        session_destroy();
        header("Location: index.php");
        exit();
    }

    $select_products = "SELECT * FROM products";
    $result_select_products= mysqli_query($conn, $select_products);

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(isset($_POST['drop'])){
            $delete_id = $_POST['delete_id'];

            $delete = "DELETE FROM products WHERE id = '$delete_id'";
            mysqli_query($conn, $delete);
            // header("Location: totalproducts.php");
            // exit;
        }
    }

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(isset($_POST['submit'])){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $price = $_POST['price'];
            $category = $_POST['productCategory'];
            $date = date("Y-m-d H:i:s");
            
            if($title != "" && $price != "" && $category != ""){
                if(isset($_FILES['my_image'])){
                    echo "<pre>";
                    print_r($_FILES['my_image']);
                    echo "</pre>";
        
                    $img_name = $_FILES['my_image']['name'];
                    $img_size = $_FILES['my_image']['size'];
                    $tmp_name = $_FILES['my_image']['tmp_name'];
                    $error = $_FILES['my_image']['error'];
        
                    if($error === 0){
                        if($img_size > 248000){
                            echo "Sorry, your file is too large.";
                        }else{
                            $img_ex = pathinfo($img_name, PATHINFO_EXTENSION);
                            $img_ex_lc = strtolower($img_ex);
        
                            $allowed_exs = array("jpg", "jpeg", "png");
        
                            if(in_array($img_ex_lc, $allowed_exs)){
                                $new_img_name = uniqid("IMG-", true). '.'.$img_ex_lc;
                                $img_upload_path = 'uploads/'. $new_img_name;
                                move_uploaded_file($tmp_name, $img_upload_path);
        
                                $insert_product = "INSERT INTO products (title, description, price, category, image, date) VALUES ('$title', '$description', '$price', '$category', '$new_img_name', '$date')";
                                $result_insert_products = mysqli_query($conn, $insert_product);
                                header("Location: totalproducts.php");
                                exit;
                            }else{
                                echo "Sorry, you can not upload this type file";
                                // header("Location: totalproducts.php");
                                // exit;
                            }
                        }
                    }else{
                        $insert_product = "INSERT INTO products (title, description, price, category, image, date) VALUES ('$title', '$description', '$price', '$category', '$new_img_name', '$date')";
                        $result_insert_products = mysqli_query($conn, $insert_product);
                        header("Location: totalproducts.php");
                        exit;
                    }
                }
            }
        }

        
    }

?>