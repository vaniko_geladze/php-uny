-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2024 at 06:54 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mymarket_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminlogin`
--

CREATE TABLE `adminlogin` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `adminpassword` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `adminlogin`
--

INSERT INTO `adminlogin` (`id`, `username`, `adminpassword`) VALUES
(1, 'admin1', '123admin');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`) VALUES
(1, 'ტექნიკა'),
(2, 'საოჯახო ტექნიკა'),
(3, 'სილამაზე და მოდა'),
(4, 'სახლი და ბაღი'),
(5, 'ბიზნესი და დანადგარები');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `product_title` varchar(50) NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `product_title`, `product_price`, `quantity`) VALUES
(1, 'Google Pixel 7', 950.00, 5),
(2, 'ტელევიზორი', 1500.00, 22),
(3, 'Iphon 11', 2300.00, 78),
(4, 'Lenovo legion', 3500.00, 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `category` varchar(30) NOT NULL,
  `image` varchar(2048) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `price`, `category`, `image`, `date`) VALUES
(96, 'გამათბობელი', '120 კუბი', 1500.00, 'საოჯახო ტექნიკა', 'IMG-65a645f458ccd3.81412052.jpg', '2024-01-16 10:01:40'),
(102, 'სარეცხის მანქანა.', 'იქოქება დადის', 501.00, 'საოჯახო ტექნიკა', 'IMG-65a650b6f14d62.23013621.jpg', '2024-01-16 10:47:34'),
(103, 'მტვერ სასრუუტ', 'კაბელის სიგრძე 15 მეტრი', 1800.00, 'საოჯახო ტექნიკა', 'IMG-65a6ed023fc132.92443880.jpg', '2024-01-16 21:54:26'),
(105, 'I7 GTX-1080TI', 'მოყვება 1 წლიანი გარანტია', 2000.00, 'ტექნიკა', 'IMG-65a6ee4c1d1333.79506819.jpg', '2024-01-16 21:59:56'),
(106, 'ჟურნალის მაგიდა', 'იყიდება ახალი ჟურნალის მაგიდა', 350.00, 'სახლი და ბაღი', 'IMG-65a80ee1cb8f93.14687335.jpg', '2024-01-17 18:31:13'),
(107, 'ჰაერის ელექტრო გამათბობელი', 'ჰაერის ელექტრო გამათბობელიო 5 KW კალორიფერი SIAL DA5', 240.00, 'ბიზნესი და დანადგარები', 'IMG-65a80f22b7b029.00083828.jpg', '2024-01-17 18:32:18'),
(108, 'iphone 6 plus ', 'ტელეფობი არის კარგ მდგომარეიბაში', 150.00, 'ტექნიკა', 'IMG-65a80f5bac0eb4.99389458.jpg', '2024-01-17 18:33:15'),
(110, 'გრილი, მაყალი', 'იყიდება პროფესიონალური გრილი (2024 წლის იანვარი) 200 გალონის მოცულობის, ხელნაკეთი გრილი ბორბლებით და ჩაშენებული დასაკეცი მაგიდით, 1,5 მეტრი სიგრძით, ორი სექცია, ორი კარი, ორი გამწოვი .', 1000.00, 'სახლი და ბაღი', 'IMG-65a81041a48604.27545314.jpg', '2024-01-17 18:37:05'),
(111, 'ASUS ROG Strix Scar 17 (2022) I9-12900H 240Hz RTX ', 'ASUS ROG Strix Scar 17 (2022) Gaming Laptop, 17.3” 240Hz IPS QHD Display, NVIDIA GeForce RTX 3070 Ti, Intel Core i9-12900H, 32GB DDR5, 1TB SSD, Per-Key RGB Keyboard, Windows 11 Pro, G733ZW-XS96', 6800.00, 'ტექნიკა', 'IMG-65a810d144ed13.09581512.jpg', '2024-01-17 18:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(20) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `name` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `phonenumber` varchar(20) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `gender`, `name`, `lastname`, `phonenumber`, `role`) VALUES
(1, 'admin@gmail.com', '123admin', 'მამრობითი', 'vaniko', 'geladze', '598307554', 'admin'),
(3, 'levani@gmail.com', 'levani123', 'მამრობითი', 'levani', 'bolotaevi', '598742563', 'user'),
(4, 'nugzar@gmail.com', 'nugzar123', 'მამრობითი', 'ნუგზარ', 'ბრუკლინსკი', '596201536', 'user'),
(5, 'elene@gmail.com', 'elene123', 'მდედრობითი', 'elene', 'adamia', '551452578', 'user'),
(18, 'vano.vanikogeladze@gmail.com', 'vaniko123', 'მამრობითი', 'Vaniko', 'Geladze', '598307554', 'user'),
(20, 'merabi@gmail.com', 'merabi123', 'მამრობითი', 'mereabi', 'merabishvili', '598121314', 'user'),
(23, 'natalia@gmail.com', 'natalia123', 'მამრობითი', 'natalia', 'narmania', '569893623', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminlogin`
--
ALTER TABLE `adminlogin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminlogin`
--
ALTER TABLE `adminlogin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
