<?php 
    session_start();
    if(isset($_SESSION['login_alert'])){
        echo "<script>alert('". $_SESSION['login_alert'] ."')</script>";
        unset($_SESSION['login_alert']);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TENET- შესვლა</title>
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Noto+Sans+Georgian:wght@200;300;400&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="page1.css">
    <link rel="shortcut icon" href="page1-photos/shortcut.png">
</head>
<body>
    <header>
        <div class="left-section">
            <img class="qr" src="page1-photos/qr-code.svg" alt="">
            <p class="tineti">თინეთი</p>
        </div>

        <div class="right-section">
            <div class="language">
                <p>ქართული</p>
                <img class="arrow-down" src="page1-photos/chevron-down-s-svgrepo-com.svg" alt="">

                <div class="select-language">
                    <button class="chose-language chose-language1">ქართული</button>
                    <br>
                    <button class="chose-language">English</button>
                    <br>
                    <button class="chose-language">Русский</button>
                    <br>
                    <button class="chose-language">Azer</button>
                    <br>
                    <button class="chose-language">Armenia</button>
                </div>
            </div>
        </div>
    </header>
    
    <div class="main">

            <div class="registration">
                <p class="crate">ანგარიშის შექმნა</p>
                <form action="../conection.php" method="post">
                    <input class="same" type="text" placeholder="ელფოსტა" name="email" required>
                    <br>
                    <input class="same"  type="password" placeholder="პაროლი" name="password" required>
                    <br>
                    <p class="foget">პაროლის აღდგენა</p>
                    <button class="submit" name="login">შესვლა</button>
                    <br>

                    <div class="sighnup-with" >
                        <button class="sighn-with" formaction="page2.php">
                            <img class="logo" src="page1-photos/google-svgrepo-com.svg" alt="">
                            <p>Google</p>
                        </button>
        
                        <button class="sighn-with" formaction="page2.php">
                            <img class="logo" src="page1-photos/facebook-svgrepo-com.svg" alt="">
                            <p>Facebook</p>
                        </button>
                    </div>

                    <p class="sighn-in">არ გაქვს ანგარიში? - <a class="link" href="page1.php">შექმენი</a> </p>
                </form>
            </div>

            <div class="left-side">
                <div class="image">
                    <img class="side-img" src="page1-photos/Screenshot 2023-01-03 145816.png" alt="">
                </div>
            </div>
    </div>

</body>
</html>