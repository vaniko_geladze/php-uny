<?php 
    session_start();
    if(isset($_SESSION['alert_message'])){
        echo "<script>alert('". $_SESSION['alert_message'] ."')</script>";
        unset($_SESSION['alert_message']);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TENET- ანგარიშის შექმნა</title>
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Noto+Sans+Georgian:wght@200;300;400&family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="page1.css">
    <link rel="shortcut icon" href="page1-photos/shortcut.png">
</head>
<body>
    <header>
        <div class="left-section">
            <img class="qr" src="page1-photos/qr-code.svg" alt="">
            <p class="tineti">თინეთი</p>
        </div>

        <div class="right-section">
            <div class="language">
                <p>ქართული</p>
                <img class="arrow-down" src="page1-photos/chevron-down-s-svgrepo-com.svg" alt="">

                <div class="select-language">
                    <button class="chose-language chose-language1">ქართული</button>
                    <br>
                    <button class="chose-language">English</button>
                    <br>
                    <button class="chose-language">Русский</button>
                    <br>
                    <button class="chose-language">Azer</button>
                    <br>
                    <button class="chose-language">Armenia</button>
                </div>
            </div>
        </div>
    </header>

    <div class="main">
        <div class="registration">
            <p class="crate">ანგარიშის შექმნა</p>
            <form action="registration.php" method="post">
                <input class="same" type="mail" name='email' placeholder="ელფოსტა" required>
                <br>
                <input class="same"  type="password" name='password' placeholder="პაროლი" required>
                <br>
                <p class="gander">სქესი</p>
                <br>
                <input type="radio" class="radio-style" id="male" name="gender" value="მამრობითი">
                <label for="gender" class="radio-text">მამრობითი</label>
                <input type="radio" class="radio-style" id="female" name="gender" value="მდედრობითი" required>
                <label for="gender" class="radio-text">მდედრობითი</label>
                <br>
                <input class="same"  type="text" name='name' placeholder="სახელი" required>
                <br>
                <input class="same"  type="text" name='lastname' placeholder="გვარი" required>
                <br>
                <input class="same"  type="number" name='phonenumber' placeholder="ტელეფონის ნომერი" required>
                <br>
                <input type="checkbox" id="permision" required>
                <label for="permision" class="checkbox-text">ვეთანხმები <span>წესებს და პირობებს</span></label>
                <br>
                <input type="checkbox" id="permision" required>
                <label for="permision1" class="checkbox-text">ვეთანხმები <span>კონფიდენციალობის პოლიტიკას</span></label>
                <br>
                <button class="submit" name="registration">დადასტურება</button>
                <br>

                <div class="sighnup-with">
                    <button class="sighn-with" formaction="page1.php">
                        <img class="logo" src="page1-photos/google-svgrepo-com.svg" alt="">
                        <p>Google</p>
                    </button>
    
                    <button class="sighn-with" formaction="page1.php">
                        <img class="logo" src="page1-photos/facebook-svgrepo-com.svg" alt="">
                        <p>Facebook</p>
                    </button>
                </div>

                <p class="sighn-in">არსებული ანგარიშით <a class="link" href="page2.php">შესვლა</a></p>
            </form>
        </div>

        <div class="left-side">
            <div class="image">
                <img class="side-img" src="page1-photos/Screenshot 2023-01-03 145816.png" alt="">
            </div>
        </div>
    </div>

</body>
</html>