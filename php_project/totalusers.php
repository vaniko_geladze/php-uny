<?php
    session_start();
    include "db_conection.php";
    
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header("Location: index.php");
        exit();
    }

    if (isset($_GET['logout'])) {
        session_unset();
        session_destroy();
        header("Location: index.php");
        exit();
    }

    $select_users = "SELECT * FROM users";
    $result_users = mysqli_query($conn, $select_users);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>admin</title>
    <link rel="stylesheet" href="styles/admin-page.css">
    <link rel="shortcut icon" href="photos/market-shortcut.png">
</head>

<header>
    <div class="nav">
        <a href="admin.php"><img src="photos/software-engineer.png" class="admin-icon"></a>
        <a href="?logout"><img src="photos/7612790.png" class="logout-icon"></a>
    </div>
</header>

<body>
    <div class="users-div">
        <table class="users-table">
            <tr>
                <th>ID</th>
                <th>Email</th>
                <th>Password</th>
                <th>Gender</th>
                <th>Name</th>
                <th>Lastname</th>
                <th>Phone number</th>
                <th>Role</th>
            </tr>
            <?php 
                foreach ($result_users as $users) {
                    echo "<tr>";
                        echo "<td>". $users['id'] . "</td>";
                        echo "<td>". $users['email'] . "</td>";
                        echo "<td>". $users['password'] . "</td>";
                        echo "<td>". $users['gender'] . "</td>";
                        echo "<td>". $users['name'] . "</td>";
                        echo "<td>". $users['lastname'] . "</td>";
                        echo "<td>". $users['phonenumber'] . "</td>";
                        echo "<td>". $users['role'] . "</td>";
                    echo "</tr>";
                }
            ?>
        </table>
    </div>
</body>
</html>