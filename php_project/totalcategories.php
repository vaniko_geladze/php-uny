<?php
    session_start();
    include "db_conection.php";
    
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header("Location: index.php");
        exit();
    }

    if (isset($_GET['logout'])) {
        session_unset();
        session_destroy();
        header("Location: index.php");
        exit();
    }

    $select_categories = "SELECT * FROM categories";
    $result_categories = mysqli_query($conn, $select_categories);
    $categories_quantity = 0;

    $sum_product = "SELECT * FROM products";
    $result_sum_products = mysqli_query($conn, $sum_product);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>admin</title>
    <link rel="stylesheet" href="styles/admin-page.css">
    <link rel="shortcut icon" href="photos/market-shortcut.png">
</head>

<header>
    <div class="nav">
        <a href="admin.php"><img src="photos/software-engineer.png" class="admin-icon"></a>
        <a href="?logout"><img src="photos/7612790.png" class="logout-icon"></a>
    </div>
</header>

<body>
<div class="users-div different">
        <table class="users-table categories-table">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Size</th>
            </tr>
            <?php 
                foreach ($result_categories as $categories) {
                    $sum = 0;
                    foreach ($result_sum_products as $row) {
                        if ($row['category'] == $categories['category_name']) {
                            $sum++;
                        }
                    }
                    echo "<tr>";
                        echo "<td>". $categories['id'] . "</td>";
                        echo "<td>". $categories['category_name'] . "</td>";
                        echo "<td>". $sum . "</td>";
                    echo "</tr>";
                }
            ?>
        </table>
    </div>
</body>
</html>