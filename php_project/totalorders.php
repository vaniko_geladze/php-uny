<?php
    session_start();
    include "db_conection.php";
    
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header("Location: index.php");
        exit();
    }

    if (isset($_GET['logout'])) {
        session_unset();
        session_destroy();
        header("Location: index.php");
        exit();
    }

    $select_users = "SELECT * FROM orders";
    $result_users = mysqli_query($conn, $select_users);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>admin</title>
    <link rel="stylesheet" href="styles/admin-page.css">
    <link rel="shortcut icon" href="photos/market-shortcut.png">
</head>

<header>
    <div class="nav">
    <a href="admin.php"><img src="photos/software-engineer.png" class="admin-icon"></a>
        <a href="?logout"><img src="photos/7612790.png" class="logout-icon"></a>
    </div>
</header>

<body>
    <div class="users-div">
        <table class="users-table">
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Sell Sum</th>
            </tr>
            <?php 
                foreach ($result_users as $users) {
                    $price = $users['product_price'];
                    $quantiti = $users['quantity'];
                    $sum = $price * $quantiti;
                    echo "<tr>";
                        echo "<td>". $users['id'] . "</td>";
                        echo "<td>". $users['product_title'] . "</td>";
                        echo "<td>". $users['product_price'] . "₾" ."</td>";
                        echo "<td>". $users['quantity'] . "</td>";
                        echo "<td>". $sum . "₾" . "</td>";
                    echo "</tr>";
                }
            ?>
        </table>
    </div>
</body>
</html>