<?php
    session_start();
    $_SESSION['post_data'] = $_POST;
    include "db_conection.php";
    
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header("Location: index.php");
        exit();
    }

    if (isset($_GET['logout'])) {
        session_unset();
        session_destroy();
        header("Location: index.php");
        exit();
    }

    $select_products = "SELECT * FROM products";
    $result_select_products= mysqli_query($conn, $select_products);

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(isset($_POST['drop'])){
            $delete_id = $_POST['delete_id'];

            $delete = "DELETE FROM products WHERE id = '$delete_id'";
            mysqli_query($conn, $delete);
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>admin</title>
    <link rel="stylesheet" href="styles/admin-page.css">
    <link rel="shortcut icon" href="photos/market-shortcut.png">
</head>

<header>
    <div class="nav">
        <a href="admin.php"><img src="photos/software-engineer.png" class="admin-icon"></a>
        <a href="?logout"><img src="photos/7612790.png" class="logout-icon"></a>
    </div>
</header>

<body>
    <div class="users-div">
        <table class="users-table">
            <tr>
                <th>ID</th>
                <th>Image</th>
                <th>Title</th>
                <th>Description</th>
                <th>Price</th>
                <th>Category</th>
                <th>Date</th>
                <th>Edit</th>
                <th>Drope</th>
            </tr>
            <?php 
                foreach ($result_select_products as $product) {
                    echo "<tr>";
                        echo "<td>". $product['id'] . "</td>";
                        echo "<td>";
                            if (!empty($product['image']) && file_exists("uploads/" . $product['image'])) {
                                echo "<img class='image' src='uploads/" . $product['image'] . "'>";
                            } else {
                                echo "<img class='image' src='uploads/default.jpg' alt='No Image'>";
                            }
                        echo "</td>";
                        echo "<td>". $product['title'] . "</td>";
                        echo "<td>"."<textarea class='test' readonly>". $product['description'] ."</textarea>" . "</td>";
                        echo "<td>". $product['price'] ."₾" . "</td>";
                        echo "<td>". $product['category'] . "</td>";
                        echo "<td>". $product['date'] . "</td>";
                        echo "<td>
                                    <form method='post' action='edit.php'>
                                        <input type='hidden' name='edit_id' value='". $product['id'] ."' />
                                        <button type='submit' name='edit' class='edit'>Edit</button>
                                    </form>
                                </td>";
                        echo "<td>
                                    <form method='post'>
                                        <input type='hidden' name='delete_id' value='". $product['id'] ."' />
                                        <button type='submit' name='drop' class='delete'>Drope</button>
                                    </form>
                                </td>";
                    echo "</tr>";
                }
            ?>
        </table>
        
        <button class="add-btn" name="add" onclick="openAddProductForm()">Add Product</button>


        <div id="addProductModal" class="modal-overlay">
            <div class="add-div">
                <h1>New Product Item</h1>
                <br>
               <form method="post" action="addproduct.php" enctype="multipart/form-data">
                    <label for="title">Product title:</label><br>
                    <input type="text" name="title" class="title" required><br>

                    <label for="description">Description:</label><br>
                    <input type="text" name="description" class="description"><br>

                    <label for="price">Price:</label><br>
                    <input type="number" name="price" class="price" required><br>

                    <label for="">Category</label>
                    <select name="productCategory" id="productCategory" required>
                        <option value="">Choose...</option>
                        <option value="ტექნიკა">ტექნიკა</option>
                        <option value="საოჯახო ტექნიკა">საოჯახო ტექნიკა</option>
                        <option value="სილამაზე და მოდა">სილამაზე და მოდა</option>
                        <option value="სახლი და ბაღი">სახლი და ბაღი</option>
                        <option value="ბიზნესი და დანადგარები">ბიზნესი და დანადგარები</option>
                    </select><br><br>

                    <label for="">Choose Image:</label><br>
                    <input type="file" name="my_image"><br>
                    <button type='submit' class="submit" name='submit'>Submit</button>
               </form>

               <button onclick="closeAddProductForm()" class="close">Close</button>
            </div>
        </div>
    </div>

    <script>
        function openAddProductForm() {
            document.getElementById("addProductModal").style.display = "flex";
        }

        function closeAddProductForm() {
            document.getElementById("addProductModal").style.display = "none";
        }

        function refreshPage() {
            location.reload();
        }
    </script>

</body>
</html>