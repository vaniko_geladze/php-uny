<?php 
    session_start();
    $_SESSION['post_data'] = $_POST;
    include "db_conection.php";
    
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header("Location: index.php");
        exit();
    }

    if (isset($_GET['logout'])) {
        session_unset();
        session_destroy();
        header("Location: totalproducts.php");
        exit();
    }include "db_conection.php";

    if($_SERVER["REQUEST_METHOD"] === "POST"){
        $edit_id = $_POST['edit_id'];

        if(isset($_POST['edit_id'])){
            $edit_seleqt = "SELECT * FROM products WHERE id='$edit_id'";
            $result_select = mysqli_query($conn, $edit_seleqt);

            if($result_select && mysqli_num_rows($result_select) > 0){
                $edit_product = mysqli_fetch_assoc($result_select);
            }
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>admin</title>
    <link rel="stylesheet" href="styles/admin-page.css">
    <link rel="shortcut icon" href="photos/market-shortcut.png">
</head>

<header>
    <div class="nav">
        <a href="admin.php"><img src="photos/software-engineer.png" class="admin-icon"></a>
        <a href="?logout"><img src="photos/7612790.png" class="logout-icon"></a>
    </div>
</header>
<body>
    <div id="addProductModal" class="edit-overlay">
            <div class="add-div">
                <h1>New Product Item</h1>
                <br>
               <form method="post" action="update.php">
                    <label for="title">Product title:</label><br>
                    <input type="hidden" name="edit_id" value="<?php echo $edit_product['id']; ?>">
                    <input type="text" name="title" class="title" value="<?php echo $edit_product['title']; ?>"><br>

                    <label for="description">Description:</label><br>
                    <input type="text" name="description" class="description" value="<?php echo $edit_product['description']; ?>"><br>

                    <label for="price">Price:</label><br>
                    <input type="number" name="price" class="price" value="<?php echo $edit_product['price']; ?>"><br>

                    <label for="">Category</label>
                    <select name="productCategory" id="productCategory" required>
                        <option value="ტექნიკა" <?php echo ($edit_product['category'] == 'ტექნიკა') ? 'selected' : ''; ?>>ტექნიკა</option>
                        <option value="საოჯახო ტექნიკა" <?php echo ($edit_product['category'] == 'საოჯახო ტექნიკა') ? 'selected' : ''; ?>>საოჯახო ტექნიკა</option>
                        <option value="სილამაზე და მოდა" <?php echo ($edit_product['category'] == 'სილამაზე და მოდა') ? 'selected' : ''; ?>>სილამაზე და მოდა</option>
                        <option value="სახლი და ბაღი" <?php echo ($edit_product['category'] == 'სახლი და ბაღი') ? 'selected' : ''; ?>>სახლი და ბაღი</option>
                        <option value="ბიზნესი და დანადგარები" <?php echo ($edit_product['category'] == 'ბიზნესი და დანადგარები') ? 'selected' : ''; ?>>ბიზნესი და დანადგარები</option>

                    </select><br><br>

                    <!-- <label for="">Choose Image:</label><br>
                    <input type="file" name="my_image"><br> -->
                    <button type='submit' class="submit" name='update'>Submit</button>
                    <button class="close" formaction="totalproducts.php">Close</button>
               </form>

               
            </div>
        </div>
</body>
</html>