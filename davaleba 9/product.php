<?php

    $id = $_POST['id'] ?? null;
    $pdo = new PDO('mysql:host=localhost;port=3306;dbname=ecommerce_db', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statment = $pdo->prepare('SELECT * FROM products WHERE id= :id');
    $statment->bindValue(':id', $id);
    $statment->execute();

    $prduct = $statment->fetch(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="products">
        <div class="car-product">
            <h1 class="cat-name"><?php echo $prduct['title'] ?></h1>
            <h1 class="cat-name">price: <?php echo $prduct['price'] ?>$</h1>
            <h3 class="cat-name"><?php echo $prduct['description'] ?></h3>
        </div>  
    </div>
</body>
</html>