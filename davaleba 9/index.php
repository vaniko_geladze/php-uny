<?php 

$pdo = new PDO('mysql:host=localhost;port=3306;dbname=ecommerce_db', 'root', '');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$statment = $pdo->prepare('SELECT * From categories');
$statment->execute();
$categories = $statment->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="flex-div">
        <?php foreach($categories as $categorie){ ?>
            <div class="categories-div">
                <h1 class="cat-name"><?php echo $categorie['name'] ?></h1>
                <form method="post" action="product.php">
                    <input type="hidden" name="id"; value="<?php echo $categorie['id'] ?>"/>
                    <button type="submit">viwe details</button>
                </form>
            </div>
        
        <?php } ?>
    </div>
</body>
</html>
