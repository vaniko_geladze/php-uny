<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style2.css">
</head>
<body>

        <form action="get-post-point3.php" method="post" id="form">
            <label for="" type="radio" name="fav_language">Task 1 <br> What does the acronym "HTML" stand for in web development?</label><br>
            <br>
            <input type="radio" id="answer1" name="HTML" value="A" required>
            <label for="answer1">A) HyperText Markup Language</label>
            <br><br>
            <input type="radio" id="answer2" name="HTML" value="B" required>
            <label for="answer2">B) Hyper Transfer Markup Language</label>
            <br><br>
            <input type="radio" id="answer3" name="HTML" value="C" required>
            <label for="answer3">C) High-Level Text Markup Language</label>
            <br><br>
            <input type="radio" id="answer4" name="HTML" value="D" required>
            <label for="answer4">D) Hyperlink and Text Markup Language</label>
            <hr>

            <label for="" type="radio" name="fav_language">Task 2 <br> What is JavaScript primarily used for in web development?</label><br>
            <br>
            <input type="radio" id="answer2.1" name="JS" value="A" required>
            <label for="answer2.1">A) Styling web pages and layouts</label>
            <br><br>
            <input type="radio" id="answer2.2" name="JS" value="B" required>
            <label for="answer2.2">B) Managing server-side databases</label>
            <br><br>
            <input type="radio" id="answer2.3" name="JS" value="C" required>
            <label for="answer2.3">C) Enhancing the interactivity and functionality of web pages</label>
            <br><br>
            <input type="radio" id="answer2.4" name="JS" value="D" required>
            <label for="answer2.4">D) Defining the structure and layout of web content</label>
            <hr>

            <label for="" type="radio" name="fav_language">Task 3 <br> What is Python primarily known for in the field of programming?</label><br>
            <br>
            <input type="radio" id="answer2.1" name="PYTHON" value="A" required>
            <label for="answer2.1">A) Creating stylish and visually appealing user interfaces</label>
            <br><br>
            <input type="radio" id="answer2.2" name="PYTHON" value="B" required>
            <label for="answer2.2">B) Writing low-level system software</label>
            <br><br>
            <input type="radio" id="answer2.3" name="PYTHON" value="C" required>
            <label for="answer2.3">C) Rapid development and ease of use</label>
            <br><br>
            <input type="radio" id="answer2.4" name="PYTHON" value="D" required>
            <label for="answer2.4">D) Data analysis and management</label>
            <hr>
            
            <label for="">Task 4</label><br>
            <label for="">What is the Python term for a named storage location that can hold a value?</label><br>
            <input type="text" name="openPython" id="openQuestion">
            <br>
            <label for="">Task 5</label><br>
            <label for="">What is the JavaScript term for a function defined inside another function?</label><br>
            <input type="text" name="openJS" id="openQuestion">
            <br><br>
            <button class="btn">Check</button>
        </form>
</body>
</html>