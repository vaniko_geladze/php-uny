<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <form action="get-post.php" method="post" id="form">
        <label for="">Name</label><br>
        <input type="text" name="userName" required>
        <br>
        <label for="">LastName</label><br>
        <input type="text" name="userLastName" required>
        <br>
        <label for="">Position</label><br>
        <input type="text" name="position" required>
        <br>
        <label for="">Salary</label><br>
        <input type="number" name="salary" required>
        <br>
        <label for="">Retained earnings in %</label><br>
        <input type="number" name="retained" required>
        <br><br>
        <button>Make Table</button>
    </form>
</body>
</html>