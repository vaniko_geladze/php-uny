<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <h1>
        <?php

            $counter = 0;

            if($_SERVER['REQUEST_METHOD'] === 'POST'){
                $userChoice1 = $_POST['HTML'];
                $userChoice2 = $_POST['JS'];
                $userChoice3 = $_POST['PYTHON'];
                $userChoice4 = $_POST['openPython'];
                $userChoice5 = $_POST['openJS'];

                $correctAnswer1 = 'A';
                $correctAnswer2 = 'C';
                $correctAnswer3 = 'C';


                if($userChoice1 === $correctAnswer1){
                    $counter += 1;
                } else {
                    $counter += 0;
                }

                if($userChoice2 === $correctAnswer2){
                    $counter += 1;
                } else {
                    $counter += 0;
                }

                if($userChoice3 === $correctAnswer3){
                    $counter += 1;
                } else {
                    $counter += 0;
                }

                if($userChoice4 === "Variable" || $userChoice4 === "variable" || $userChoice4 === "Variables" || $userChoice4 === "variables" ){
                    $counter += 1;
                } else {
                    $counter += 0;
                }

                if($userChoice5 === "Closure" || $userChoice5 === "closure"){
                    $counter += 1;
                } else {
                    $counter += 0;
                }


                echo "You got $counter point";
            }
        ?>
    </h1>

</body>
</html>