<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="main">
        <form action="engine.php" method="post">
            <label for="title">სტატიის სათაური</label><br>
            <input type="text" name="title"><br>

            <label for="date">სტატიის თარიღი</label><br>
            <input type="date" name="date"><br>

            <label for="name">ავტორის სახელი და გვარი</label><br>
            <input type="text" name="name"><br>

            <label for="text">სტატიის ტექსტი</label><br>
            <!-- <input type="text" name="text"><br> -->
            <div class="input-container">
                <textarea id="auto-resize" cols="94" rows="2"  name="text" placeholder="Type your text here..."></textarea>
            </div>

            <button>SEND</button>
        </form>
    </div>
</body>
</html>


<script>
    const textarea = document.getElementById("auto-resize");

    textarea.addEventListener("input", function () {
        this.style.height = "auto";
        this.style.height = (this.scrollHeight) + "px";
    });
</script>
